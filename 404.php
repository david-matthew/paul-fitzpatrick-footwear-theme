<?php
/**
 * The template for displaying 404 pages
 *
 * @package Paul Fitzpatrick Footwear
 */

get_header();
?>

<div id="primary" class="content-area container">

	<main id="main" class="site-main">

		<div class="row" data-aos="fade-up">

			<div class="col-xs-12 col-md-offset-3 col-md-6">

				<section class="error-404 not-found">

					<header class="page-header">

						<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'pf-footwear' ); ?></h1>

					</header>

					<div class="page-content">
						<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'pf-footwear' ); ?></p>

						<?php get_search_form(); ?>

					</div><!-- .page-content -->

				</section><!-- .error-404 -->

			</div>

		</div>

	</main>

</div><!-- #primary .container -->

<?php

get_template_part( 'template-parts/have-a-question' );

get_footer();

