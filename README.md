# Paul Fitzpatrick Footwear

A custom Woocommerce-based Wordpress theme designed and developed for the Paul Fitzpatrick Footwear website. 

## Coding Standard
This theme follows the [Wordpress Coding Standard](https://codex.wordpress.org/WordPress_Coding_Standards).

## Libraries
* Bootstrap v3.3.7
* Fontawesome v4.7
* Animate-on-scroll v2.3.1
* Owl Carousel v2.3.4
![Paul Fitzpatrick Footwear](https://mir-s3-cdn-cf.behance.net/project_modules/fs/9745cc77774107.5c916fdfb2a58.png "Paul Fitzpatrick Footwear")
For more visuals, see [here](https://www.behance.net/gallery/77774107/Paul-Fitzpatrick-Footwear-Website).