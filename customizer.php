<?php
/**
 * Theme Customizer.
 *
 * @package Paul Fitzpatrick Footwear
 */

/**
 * Sections, settings and controls will be added here.
 *
 * @param object $wp_customize The WP Customize object.
 */
function pff_theme_options( $wp_customize ) {
	$wp_customize->remove_section( 'custom_css' );
	$wp_customize->remove_section( 'static_front_page' );
	$wp_customize->remove_panel( 'woocommerce' );

	$wp_customize->add_section(
		'carousel_settings',
		array(
			'title'       => __( 'Carousel Section', 'pf-footwear' ),
			'capability'  => 'edit_theme_options',
			'description' => __( 'Set up the Carousel (featured products) here.', 'pf-footwear' ),
		)
	);

	$wp_customize->add_setting(
		'carousel_prod_id_1',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'carousel_prod_id_1_ctr',
			array(
				'label'    => __( 'The ID of the first product to feature on the carousel', 'pf-footwear' ),
				'type'     => 'text',
				'section'  => 'carousel_settings',
				'settings' => 'carousel_prod_id_1',
			)
		)
	);

	$wp_customize->add_setting(
		'carousel_prod_id_2',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'carousel_prod_id_2_ctr',
			array(
				'label'    => __( 'The ID of the second product to feature on the carousel', 'pf-footwear' ),
				'type'     => 'text',
				'section'  => 'carousel_settings',
				'settings' => 'carousel_prod_id_2',
			)
		)
	);

	$wp_customize->add_setting(
		'carousel_prod_id_3',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'carousel_prod_id_3_ctr',
			array(
				'label'    => __( 'The ID of the third product to feature on the carousel', 'pf-footwear' ),
				'type'     => 'text',
				'section'  => 'carousel_settings',
				'settings' => 'carousel_prod_id_3',
			)
		)
	);

	$wp_customize->add_setting(
		'carousel_prod_id_4',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'carousel_prod_id_4_ctr',
			array(
				'label'    => __( 'The ID of the fourth product to feature on the carousel', 'pf-footwear' ),
				'type'     => 'text',
				'section'  => 'carousel_settings',
				'settings' => 'carousel_prod_id_4',
			)
		)
	);

	$wp_customize->add_setting(
		'carousel_prod_id_5',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'carousel_prod_id_5_ctr',
			array(
				'label'    => __( 'The ID of the fifth product to feature on the carousel', 'pf-footwear' ),
				'type'     => 'text',
				'section'  => 'carousel_settings',
				'settings' => 'carousel_prod_id_5',
			)
		)
	);

	$wp_customize->add_section(
		'collections_settings',
		array(
			'title'       => __( 'Collections Section', 'pf-footwear' ),
			'capability'  => 'edit_theme_options',
			'description' => __( 'Customise the Collections section here.', 'pf-footwear' ),
		)
	);

	$wp_customize->add_setting(
		'women_link',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'women_link_ctr',
			array(
				'label'    => __( 'The link to footwear for women.', 'pf-footwear' ),
				'type'     => 'url',
				'section'  => 'collections_settings',
				'settings' => 'women_link',
			)
		)
	);

	$wp_customize->add_setting(
		'men_link',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'men_link_ctr',
			array(
				'label'    => __( 'The link to footwear for men.', 'pf-footwear' ),
				'type'     => 'url',
				'section'  => 'collections_settings',
				'settings' => 'men_link',
			)
		)
	);

	$wp_customize->add_setting(
		'brands_link',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'brands_link_ctr',
			array(
				'label'    => __( 'The link to the brands section.', 'pf-footwear' ),
				'type'     => 'url',
				'section'  => 'collections_settings',
				'settings' => 'brands_link',
			)
		)
	);

	$wp_customize->add_section(
		'who_we_are_settings',
		array(
			'title'       => __( 'Who We Are Section', 'pf-footwear' ),
			'capability'  => 'edit_theme_options',
			'description' => __( 'Customise the Who We Are section here.', 'pf-footwear' ),
		)
	);

	$wp_customize->add_setting(
		'who_we_are_text',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'who_we_are_text_ctr',
			array(
				'label'    => __( 'The text beneath the Who We Are heading.', 'pf-footwear' ),
				'type'     => 'textarea',
				'section'  => 'who_we_are_settings',
				'settings' => 'who_we_are_text',
			)
		)
	);

	$wp_customize->add_section(
		'footer_settings',
		array(
			'title'       => __( 'Footer Section', 'pf-footwear' ),
			'capability'  => 'edit_theme_options',
			'description' => __( 'Customise the footer section here.', 'pf-footwear' ),
		)
	);

	$wp_customize->add_setting(
		'contact_link',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'contact_link_ctr',
			array(
				'label'    => __( 'The link to the contact page.', 'pf-footwear' ),
				'type'     => 'url',
				'section'  => 'footer_settings',
				'settings' => 'contact_link',
			)
		)
	);

	$wp_customize->add_setting(
		't&cs_link',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			't&cs_link_ctr',
			array(
				'label'    => __( 'The link to the terms and conditions page.', 'pf-footwear' ),
				'type'     => 'url',
				'section'  => 'footer_settings',
				'settings' => 't&cs_link',
			)
		)
	);

	$wp_customize->add_setting(
		'privacy_link',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'privacy_link_ctr',
			array(
				'label'    => __( 'The link to the privacy policy page.', 'pf-footwear' ),
				'type'     => 'url',
				'section'  => 'footer_settings',
				'settings' => 'privacy_link',
			)
		)
	);

	$wp_customize->add_setting(
		'cookies_link',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'cookies_link_ctr',
			array(
				'label'    => __( 'The link to the cookies page/section.', 'pf-footwear' ),
				'type'     => 'url',
				'section'  => 'footer_settings',
				'settings' => 'cookies_link',
			)
		)
	);

	$wp_customize->add_setting(
		'insta_link',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'insta_link_ctr',
			array(
				'label'    => __( 'Instagram profile link.', 'pf-footwear' ),
				'type'     => 'url',
				'section'  => 'footer_settings',
				'settings' => 'insta_link',
			)
		)
	);

	$wp_customize->add_setting(
		'fb_link',
		array()
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'fb_link_ctr',
			array(
				'label'    => __( 'Facebook profile link.', 'pf-footwear' ),
				'type'     => 'url',
				'section'  => 'footer_settings',
				'settings' => 'fb_link',
			)
		)
	);
}
add_action( 'customize_register', 'pff_theme_options' );
