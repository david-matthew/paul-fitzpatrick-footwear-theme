<?php
/**
 * The template for displaying the footer.
 *
 * @package Paul Fitzpatrick Footwear
 */

?>

<footer>

	<div class="container footer-sct-1">

		<div class="row text-center">

			<div class="col-xs-12">

				<a href="<?php echo esc_url( get_theme_mod( 't&cs_link' ) ); ?>">Terms & Conditions</a>

				<a href="<?php echo esc_url( get_theme_mod( 'privacy_link' ) ); ?>">Privacy Policy</a>

				<a href="<?php echo esc_url( get_theme_mod( 'cookies_link' ) ); ?>">Cookies</a>

				<span style="margin-left: 15px;">Stay Connected: 

					<a class="social" href="<?php echo esc_url( get_theme_mod( 'insta_link' ) ); ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>

					<a class="social" href="<?php echo esc_url( get_theme_mod( 'fb_link' ) ); ?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>

				</span>

			</div>

		</div>

	</div>

	<div class="container-fluid gold-gradient-bg footer-sct-2">

		<div class="container">

			<div class="row text-center">

				<div class="col-xs-12">

					<span>&copy; Paul Fitzpatrick Footwear <?php echo esc_html( date( 'Y' ) ); ?></span>

					<a href="//davidmatthew.ie" target="_blank">Website by David Matthew</a>

				</div>

			</div>

		</div>

	</div>

</footer>

<?php

wp_footer();
get_template_part( 'template-parts/search-modal' );


?>

</body>

</html>
