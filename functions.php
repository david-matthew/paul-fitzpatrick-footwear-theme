<?php
/**
 * Paul Fitzpatrick Footwear functions and definitions.
 *
 * @package Paul Fitzpatrick Footwear
 */

/**
 * Main setup function.
 */
function pff_setup_theme() {
	// Enable support for non-hard-coded title tags.
	add_theme_support( 'title-tag' );

	// Add support for post thumbnails.
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array( 'primary' => esc_html__( 'Primary', 'pf-footwear' ) ) );

	// Switch default core markup.
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Add support for core custom logo.
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);

	// Add support for woocommerce.
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'pff_setup_theme' );

/**
 * Load the theme scripts and styles
 *
 * @since 1.0.0
 */
function pff_load_resources() {
	// Load 3rd party styles.
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/libs/css/bootstrap.min.css', array(), '3.3.7' );
	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/libs/css/font-awesome.min.css', array(), '4.7' );
	wp_enqueue_style( 'roboto-condensed', '//fonts.googleapis.com/css?family=Roboto+Condensed:400,700', array(), '1.0' );
	wp_enqueue_style( 'owl', get_template_directory_uri() . '/libs/css/owl.carousel.min.css', array(), '2.3.4' );
	wp_enqueue_style( 'owl-default', get_template_directory_uri() . '/libs/css/owl.theme.default.min.css', array(), '2.3.4' );
	wp_enqueue_style( 'aos', get_template_directory_uri() . '/libs/css/aos.min.css', array(), '2.3.1' );

	// Load custom styles.
	wp_enqueue_style( 'pf-footwear', get_stylesheet_uri(), array( 'bootstrap' ), '1.0' );

	// Load jQuery in the footer.
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, '1.12.4', true );
	wp_enqueue_script( 'jquery' );

	// Load 3rd party scripts.
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/libs/js/bootstrap.min.js', array( 'jquery' ), '3.3.7', true );
	wp_enqueue_script( 'owl', get_template_directory_uri() . '/libs/js/owl.carousel.min.js', array( 'jquery' ), '2.3.4', true );
	wp_enqueue_script( 'aos', get_template_directory_uri() . '/libs/js/aos.min.js', array(), '2.3.1', true );

	// Load custom scripts.
	wp_enqueue_script( 'general', get_template_directory_uri() . '/js/general.js', array(), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'pff_load_resources' );


/**
 * Remove unnecessary admin menus
 *
 * @since 1.0.0
 */
function pff_remove_menus() {
	remove_menu_page( 'edit.php' );
	remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'pff_remove_menus' );


/**
 * Include additional functions
 *
 * @since 1.0.0
 */
get_template_part( 'customizer' );
