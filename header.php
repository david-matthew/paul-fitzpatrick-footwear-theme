<?php
/**
 * The template for displaying the header.
 *
 * @package Paul Fitzpatrick Footwear
 */

?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo esc_url( get_template_directory_uri() . '/favicon-32x32.png' ); ?>">

		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo esc_url( get_template_directory_uri() . '/favicon-16x16.png' ); ?>">

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<nav id="navbar" class="navbar navbar-default navbar-fixed-top">

			<div class="container">

				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-target" aria-expanded="false" aria-controls="navbar">

						<span class="sr-only">Toggle navigation</span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

					</button>

					<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">

						<img id="pf-logo-desktop" src="<?php echo esc_url( get_template_directory_uri() . '/img/pf-logo.svg' ); ?>" alt="Paul Fitzpatrick Footwear">

						<img id="pf-logo-mobile" src="<?php echo esc_url( get_template_directory_uri() . '/img/pf-logo-m.svg' ); ?>" alt="Paul Fitzpatrick Footwear">

					</a>

				</div>

				<div id="navbar-target" class="navbar-collapse collapse">

					<ul class="nav navbar-nav navbar-right">

						<?php

						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'menu_id'        => 'primary-menu',
								'container'      => false,
								'items_wrap'     => '%3$s',
							)
						);

						?>

						<li class="menu-item">  

							<a role="button" data-toggle="modal" data-target="#search-modal">Search</a>

						</li>

					</ul>        

				</div><!-- /.navbar-collapse -->

			</div><!-- /.container -->

		</nav>
