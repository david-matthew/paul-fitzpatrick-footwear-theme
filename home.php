<?php
/**
 * Template Name: Homepage
 *
 * @package Paul Fitzpatrick Footwear
 */

get_header(); ?>

<div class="container-fluid menu-offset gold-gradient-bg home-box-padding">

	<div class="container">

		<div class="row" style="margin-top: 30px;">

			<div id="click-collect-container" class="col-sm-12 col-lg-4">

				<h1 class="home-title uppercase">

					<div class="text-behind white-tr" data-aos="fade-right">Life is Short</div>

					<div class="text-front" data-aos="fade-up">Buy the Shoes</div>

				</h1>

				<p data-aos="fade-up" id="popular-now" class="uppercase white-tr">Popular Right Now<i class="fa fa-long-arrow-right" aria-hidden="true"></i></p>

			</div>

			<?php get_template_part( 'template-parts/carousel' ); ?>

		</div>

	</div><!-- end .container -->

</div><!-- end .container-fluid -->

<?php get_template_part( 'template-parts/collections' ); ?>

<?php get_template_part( 'template-parts/who-where' ); ?>

<div id="instagram-section" class="container home-box-padding ">

	<div class="row">

		<div class="col-sm-12" data-aos="fade-up">

			<h1 class="home-title uppercase">

				<div class="text-behind light-grey">Follow Our</div>

				<div class="text-front">Instagram</div>

			</h1>

			<?php echo do_shortcode( '[instagram-feed]' ); ?>

		</div>

	</div>

</div><!-- end .container -->

<?php get_template_part( 'template-parts/have-a-question' ); ?>

<?php get_footer(); ?>
