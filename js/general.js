// When the user scrolls down 90px from the top, adjust navbar padding
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 90 || document.documentElement.scrollTop > 90) {
        document.getElementById("navbar").style.paddingTop = "10px";
        document.getElementById("navbar").style.paddingBottom = "10px";
        
    } else {
        document.getElementById("navbar").style.paddingTop = "20px";
        document.getElementById("navbar").style.paddingBottom = "20px";
        
    }
}

// Initialize the AOS library
AOS.init({
    duration: 1200,
    offset: 180
});

// Owl Carousel Settings
jQuery(document).ready(function($) {
    $(".owl-carousel").owlCarousel( {
        autoplay: true,
        loop: true,
        margin: 25,
        dotsEach: true,
        responsive: {
            0       :   { items: 1 },
            650     :   { items: 2 },
            780     :   { items: 3 }
        }
    });
});