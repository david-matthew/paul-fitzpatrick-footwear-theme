<?php
/**
 * Template Name: Brands
 *
 * @package Paul Fitzpatrick Footwear
 */

get_header();
?>

<div id="primary" class="content-area container">

	<main id="main" class="site-main">

		<div class="row" data-aos="fade-up">

			<div class="col-xs-12 col-md-offset-3 col-md-6">

			<?php

			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', 'page' );
			endwhile;

			$brand = get_term_by( 'slug', 'brand', 'product_cat' );

			$brand_id = $brand->term_id;

			$subcategories = get_terms( 'product_cat', array( 'child_of' => $brand_id ) );

			foreach ( $subcategories as $subcategory ) {
				echo '<a class="btn-brown btn-brand" href="' . esc_url( get_term_link( $subcategory, 'product_cat' ) ) . '">' . esc_html( $subcategory->name ) . '</a>';
			}

			?>

			</div>

		</div>

	</main>

</div><!-- #primary .container -->

<?php

get_template_part( 'template-parts/have-a-question' );

get_footer();
