<?php
/**
 * Template Name: Holding Page.
 *
 * @package Paul Fitzpatrick Footwear
 */

?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>

	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<title>Paul Fitzpatrick - Footwear Since 1973</title>

		<style type="text/css">

		body, td, th {
			color: #FFFFFF;
		}

		body {
			background-color: #fdb913;
		}

		</style>
	</head>

	<body>

		<table align="center"cellpadding="0" cellspacing="0" border="0" width="600" style="margin-top:80px;" >

			<tr>

				<td><img src="<?php echo esc_url( get_template_directory_uri() . '/img/ffitzhold4.png' ); ?>" width="600" height="600" border="0" usemap="#Map" /></td>

			</tr>

		</table>

		<map name="Map" id="Map">

			<area shape="rect" coords="224,465,375,500" href="mailto:pfitzshoes@gmail.com" />

		</map>

	</body>

</html>
