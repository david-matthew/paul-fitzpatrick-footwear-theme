<?php
/**
 * The template for displaying all pages.
 *
 * @package Paul Fitzpatrick Footwear
 */

get_header();
?>

<div id="primary" class="content-area container">

	<main id="main" class="site-main">

		<div class="row" data-aos="fade-up">

			<div class="col-xs-12 col-md-offset-3 col-md-6">

			<?php

			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', 'page' );
			endwhile;

			?>

			</div>

		</div>

	</main>

</div><!-- #primary .container -->

<?php

get_template_part( 'template-parts/have-a-question' );

get_footer();
