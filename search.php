<?php
/**
 * The template for displaying search results
 *
 * @package Paul Fitzpatrick Footwear
 */

get_header();
?>

<section id="primary" class="content-area">

	<main id="main" class="site-main">

		<div class="row" data-aos="fade-up">

			<div class="col-xs-12 col-md-offset-3 col-md-6">

				<?php

				if ( have_posts() ) : ?>

					<header class="page-header">

						<h1 class="page-title">
							<?php
							/* translators: %s: search query. */
							printf( esc_html__( 'Search Results for: %s', 'pf-footwear' ), '<span>' . get_search_query() . '</span>' );
							?>
						</h1>

					</header>

					<?php

					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content', 'search' );
					endwhile;
					the_posts_navigation();
				else :
					get_template_part( 'template-parts/content', 'none' );
				endif;

				?>

			</div>

		</div>

	</main>

</section><!-- #primary -->

<?php
get_footer();
