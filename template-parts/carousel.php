<?php
/**
 * The template part for the homepage carousel.
 *
 * @package Paul Fitzpatrick Footwear
 */

// Get the product IDs.
$id_1 = get_post( get_theme_mod( 'carousel_prod_id_1' ) )->ID;
$id_2 = get_post( get_theme_mod( 'carousel_prod_id_2' ) )->ID;
$id_3 = get_post( get_theme_mod( 'carousel_prod_id_3' ) )->ID;
$id_4 = get_post( get_theme_mod( 'carousel_prod_id_4' ) )->ID;
$id_5 = get_post( get_theme_mod( 'carousel_prod_id_5' ) )->ID;

// Get the images via the IDs.
$carousel_img_1 = get_the_post_thumbnail_url( $id_1, 'medium' );
$carousel_img_2 = get_the_post_thumbnail_url( $id_2, 'medium' );
$carousel_img_3 = get_the_post_thumbnail_url( $id_3, 'medium' );
$carousel_img_4 = get_the_post_thumbnail_url( $id_4, 'medium' );
$carousel_img_5 = get_the_post_thumbnail_url( $id_5, 'medium' );

// Get the links via the IDs.
$carousel_link_1 = get_permalink( $id_1 );
$carousel_link_2 = get_permalink( $id_2 );
$carousel_link_3 = get_permalink( $id_3 );
$carousel_link_4 = get_permalink( $id_4 );
$carousel_link_5 = get_permalink( $id_5 );

?>

<div id="carousel-box" class="col-sm-12 col-lg-8" data-aos="fade-up">

	<div class="owl-carousel owl-theme">

		<a href="<?php echo esc_url( $carousel_link_1 ); ?>">

			<div class="item featured-img">

				<img src="<?php echo esc_url( $carousel_img_1 ); ?>" />

			</div>

		</a>

		<a href="<?php echo esc_url( $carousel_link_2 ); ?>">

			<div class="item featured-img">

				<img src="<?php echo esc_url( $carousel_img_2 ); ?>" />

			</div>

		</a>

		<a href="<?php echo esc_url( $carousel_link_3 ); ?>">

			<div class="item featured-img">

				<img src="<?php echo esc_url( $carousel_img_3 ); ?>" />

			</div>

		</a>

		<a href="<?php echo esc_url( $carousel_link_4 ); ?>">

			<div class="item featured-img">

				<img src="<?php echo esc_url( $carousel_img_4 ); ?>" />

			</div>

		</a>

		<a href="<?php echo esc_url( $carousel_link_5 ); ?>">

			<div class="item featured-img">

				<img src="<?php echo esc_url( $carousel_img_5 ); ?>" />

			</div>

		</a>

	</div>

</div>
