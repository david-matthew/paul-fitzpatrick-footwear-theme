<?php
/**
 * The template part for the homepage collections section
 *
 * @package Paul Fitzpatrick Footwear
 */

?>
<div class="container home-box-padding">

	<div class="row">

		<div class="col-sm-12">

			<h1 class="home-title uppercase">

				<div class="text-behind light-grey" data-aos="fade-up">Browse Our</div>

				<div class="text-front" data-aos="fade-up">Collections</div>

			</h1>

		</div>

	</div>

	<div id="collections" class="row" data-aos="fade-up">

		<div class="col-xs-12 col-md-4">

			<a href="<?php echo esc_url( get_theme_mod( 'women_link' ) ); ?>">

				<div class="collections-img-wrap">

					<div class="collections-img-overlay"></div>

					<div class="collections-heading text-center">

						<h2 class="uppercase">Women</h2>

					</div>

					<i class="fa fa-search" aria-hidden="true"></i>

					<img class="img img-responsive" src="<?php echo esc_url( get_template_directory_uri() . '/img/womens.jpg' ); ?>" alt="Women">

				</div>

			</a>

		</div>

		<div class="col-xs-12 col-md-4">

			<a href="<?php echo esc_url( get_theme_mod( 'men_link' ) ); ?>">

				<div class="collections-img-wrap">

					<div class="collections-img-overlay"></div>

					<div class="collections-heading text-center">

						<h2 class="uppercase">Men</h2>

					</div>

					<i class="fa fa-search" aria-hidden="true"></i>

					<img class="img img-responsive" src="<?php echo esc_url( get_template_directory_uri() . '/img/mens.jpg' ); ?>" alt="Men">

				</div>

			</a>

		</div>

		<div class="col-xs-12 col-md-4">

			<a href="<?php echo esc_url( get_theme_mod( 'brands_link' ) ); ?>">

				<div class="collections-img-wrap">

					<div class="collections-img-overlay"></div>

					<div class="collections-heading text-center">

						<h2 class="uppercase">Brands</h2>

					</div>

					<i class="fa fa-search" aria-hidden="true"></i>

					<img class="img img-responsive" src="<?php echo esc_url( get_template_directory_uri() . '/img/brands.jpg' ); ?>" alt="Brands">

				</div>

			</a>

		</div>

	</div>

</div><!-- end .container -->
