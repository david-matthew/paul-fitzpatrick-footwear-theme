<?php
/**
 * The template part for the homepage contact section.
 *
 * @package Paul Fitzpatrick Footwear
 */

?>

<div id="have-a-question" class="container-fluid gold-gradient-bg home-box-padding">

	<div class="container">

		<div class="row">

			<div class="col-xs-12 col-sm-offset-1 col-sm-5 col-md-offset-2 col-md-4" data-aos="fade-up">

				<h1 class="home-title uppercase">

					<div class="text-behind white-tr">Have a</div>

					<div class="text-front">Question?</div>

				</h1>

				<a class="btn-brown uppercase" href="<?php echo esc_url( get_theme_mod( 'contact_link' ) ); ?>">Contact Us</a>

			</div>

			<div class="col-xs-12 col-sm-6" data-aos="fade-up">

				<img id="logo-full" src="<?php echo esc_url( get_template_directory_uri() . '/img/pf-logo-full.svg' ); ?>" alt="Paul Fitzpatrick, Footwear Since 1973" />

			</div>

		</div>

	</div><!-- end .container -->

</div><!-- end .container-fluid -->
