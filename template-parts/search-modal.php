<?php
/**
 * The template for displaying the search modal dialog
 *
 * @package Paul Fitzpatrick Footwear
 */

?>

<div class="modal fade" id="search-modal" role="dialog">

	<div class="modal-dialog">
  
		<div class="modal-content">

			<div class="modal-body">

				<?php get_search_form(); ?>

			</div>

		</div><!-- end modal-content -->

	</div>

</div><!-- end modal -->
