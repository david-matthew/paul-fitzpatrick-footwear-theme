<?php
/**
 * The template part for the homepage who/where we are section
 *
 * @since   1.0.0
 */
?>

<div id="who-where" class="container-fluid menu-offset gold-gradient-bg home-box-padding">

	<div class="container">

		<div class="row">

			<div id="who-we-are"></div>

			<div class="col-xs-12" data-aos="fade-up">

				<h1 class="home-title uppercase">

					<div class="text-behind white-tr">Who</div>
					
					<div class="text-front">We Are</div>
				
				</h1>

			</div>

		</div>

		<div class="row">

			<div class="col-sm-12 col-lg-6" data-aos="fade-up">

				<?php echo get_theme_mod( 'who_we_are_text' ); ?>

			</div>

			<div class="col-sm-12 col-lg-6" data-aos="fade-up">

				<div id="who-carousel" class="carousel slide" data-ride="carousel">

					<ol class="carousel-indicators">

						<li data-target="#who-carousel" data-slide-to="0" class="active"></li>

						<li data-target="#who-carousel" data-slide-to="1"></li>

						<li data-target="#who-carousel" data-slide-to="2"></li>
					
					</ol>

  
					<div class="carousel-inner">

						<div class="item active">

							<img src="<?php echo get_template_directory_uri() . '/img/about-1.jpg'; ?>" alt="Paul Fitzpatrick Footwear - Our Shop">

						</div>

						<div class="item">
							
							<img src="<?php echo get_template_directory_uri() . '/img/about-2.jpg'; ?>" alt="Paul Fitzpatrick Footwear - Our Shop">
						
						</div>

						<div class="item">

							<img src="<?php echo get_template_directory_uri() . '/img/about-3.jpg'; ?>" alt="Paul Fitzpatrick Footwear - Our Shop">

						</div>

					</div>
			
				</div><!-- end #who-carousel -->

			</div>

		</div>

		<div class="row">

			<div id="where-we-are"></div>

			<div class="col-xs-12" data-aos="fade-up">

				<h1 class="home-title uppercase">

					<div class="text-behind white-tr">Where</div>
					
					<div class="text-front">We Are</div>
				
				</h1>

			</div>

			<div id="address" class="col-sm-12 col-lg-5" data-aos="fade-up">

				<p>Our store address:</p>

				<p>Unit 3 Level 1<br>Dundrum Town Centre<br>Sandyford Road<br>Dublin 16</p>

				<p><a target="_blank" href="https://www.google.ie/maps/dir//Paul+Fitzpatrick+Footwear,+Dundrum+Town+Centre,+Sandyford+Rd,+Dundrum,+Dublin+16/@53.2876585,-6.2429987,18.75z/data=!4m8!4m7!1m0!1m5!1m1!1s0x48670971f7c894fd:0x45d03e6274bc2a09!2m2!1d-6.2425874!2d53.2877761">Get Directions</a></p>
				
				<p>(01) 298 3270<br>
				(01) 298 3273</p>

			</div>

			<div class="col-sm-12 col-lg-7" data-aos="fade-up">

				<iframe id="open-map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-6.247701644897462%2C53.28596382242906%2C-6.238743066787721%2C53.28901674373419&amp;layer=mapnik"></iframe>
			
			</div>

		</div>

	</div><!-- end .container -->

</div><!-- end .container-fluid -->
